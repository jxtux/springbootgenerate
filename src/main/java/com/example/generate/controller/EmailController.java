package com.example.generate.controller;

import com.example.generate.SpringBootGenerateApplication;
import com.example.generate.constante.ConstantesHtml;
import com.example.generate.util.UtilApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

    @Autowired
    public UtilApp utilApp;

    static final Logger LOG = LoggerFactory.getLogger(SpringBootGenerateApplication.class);

    @GetMapping("/public/email/sendEmailTexto")
    public String getEmailSend() {

        LOG.info("enviando Correo.....");
        utilApp.testMailsendSimpleMessage("cero.one.x@gmail.com", "Hola Test Spring Mail", "hola mundo");
        LOG.info("Se envio el correo.....");

        return " email enviado......";
    }

    @GetMapping("/public/email/sendEmailHtml")
    public String getEmailSendHtml() {

        LOG.info("enviando Correo.....");
        utilApp.testMailsendSimpleMessageHtml("cero.one.x@gmail.com", "Test Html Spring Mail", ConstantesHtml.htmlFormat01);
        LOG.info("Se envio el correo.....");

        return " email enviado......";
    }

    @GetMapping("/public/email/sendEmailHtmlAdjunto")
    public String getEmailSendHtmlAdjunto() {

        LOG.info("enviando Correo.....");
        utilApp.testMailsendSimpleMessageHtmlAdjunto("cero.one.x@gmail.com", "Test Html ADJUNTO Spring Mail", "te envio un adjunto", 
                "C:\\Java\\Java_Architecture\\Cibertec\\data\\JAAD-2016-III\\02_programas\\esb\\jbossesb-4.10\\docs\\wiki\\SOAPProcessor.pdf");
        LOG.info("Se envio el correo.....");

        return " email enviado......";
    }
}
