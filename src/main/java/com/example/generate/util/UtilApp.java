package com.example.generate.util;

import com.example.generate.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

@Component
public class UtilApp {

    @Autowired
    public EmailService emailService;

    static final Logger LOG = LoggerFactory.getLogger(UtilApp.class);

    public static void testLogMesaggeError() {
        LOG.error("error Package 02 Spring Boot........ :)-");
    }

    public void testMailsendSimpleMessage(String to, String subject, String text) {
        emailService.sendSimpleMessage(to, subject, text);
    }

    public void testMailsendSimpleMessageHtml(String to, String subject, String htmlMsg) {
        emailService.sendMessageWithAttachmentHtml(to, subject, htmlMsg);
    }

    public void testMailsendSimpleMessageHtmlAdjunto(String to, String subject, String text, String adjunto) {
        emailService.sendMessageWithAttachmentHtmlAdjunto(to, subject, text, adjunto);
    }

}
