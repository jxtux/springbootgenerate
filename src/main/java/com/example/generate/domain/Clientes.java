package com.example.generate.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author User
 */
@Entity
@Table(name = "CLIENTES")
public class Clientes implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUMCLID")
    private Integer numclid;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMCLID")
    private String nomclid;
    
    @Column(name = "COD_POSTAL")
    private Integer codPostal;
    
    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;

    public Clientes() {
    }

    public Clientes(Integer numclid) {
        this.numclid = numclid;
    }

    public Clientes(Integer numclid, String nomclid) {
        this.numclid = numclid;
        this.nomclid = nomclid;
    }

    public Integer getNumclid() {
        return numclid;
    }

    public void setNumclid(Integer numclid) {
        this.numclid = numclid;
    }

    public String getNomclid() {
        return nomclid;
    }

    public void setNomclid(String nomclid) {
        this.nomclid = nomclid;
    }

    public Integer getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(Integer codPostal) {
        this.codPostal = codPostal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numclid != null ? numclid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clientes)) {
            return false;
        }
        Clientes other = (Clientes) object;
        if ((this.numclid == null && other.numclid != null) || (this.numclid != null && !this.numclid.equals(other.numclid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.generate.SpringBootGenerate.Clientes[ numclid=" + numclid + " ]";
    }
    
}
