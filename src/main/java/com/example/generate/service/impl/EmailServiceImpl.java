package com.example.generate.service.impl;

import com.example.generate.SpringBootGenerateApplication;
import com.example.generate.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class EmailServiceImpl implements EmailService {

    @Autowired
    public JavaMailSender emailSender;

    static final Logger LOG = LoggerFactory.getLogger(SpringBootGenerateApplication.class);

    @Override
    public void sendSimpleMessage(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);

            emailSender.send(message);

        } catch (MailException exception) {
            //exception.printStackTrace();
        }
    }

    @Override
    public void sendMessageWithAttachmentHtml(String to, String subject, String htmlMsg) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, false, "utf-8");

            message.setContent(htmlMsg, "text/html");
            helper.setTo(to);
            helper.setSubject(subject);

            emailSender.send(message);

        } catch (MessagingException e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void sendMessageWithAttachmentHtmlAdjunto(String to, String subject, String text, String pathToAttachment) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");

            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);

            FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
            helper.addAttachment(file.getFilename(), file);

            emailSender.send(message);

        } catch (MessagingException e) {
            LOG.error("error File: " + e.getMessage());
        }
    }
}
