package com.example.generate.service;

public interface EmailService {

    void sendSimpleMessage(String to, String subject, String text);

    void sendMessageWithAttachmentHtml(String to, String subject, String htmlMsg);

    void sendMessageWithAttachmentHtmlAdjunto(String to, String subject, String text, String pathToAttachment);

}
